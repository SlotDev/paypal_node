const express = require('express')
const bodyParser = require('body-parser')
const ejs = require('ejs')
const paypal = require('paypal-rest-sdk')
const dotenv = require('dotenv')
dotenv.config()

paypal.configure({
  /*
  'mode': 'live', //sandbox or live
  'client_id': process.env.clientIdLive,
  'client_secret': process.env.clientSecretLive
  */ 
  
  'mode': 'sandbox', //sandbox or live
  'client_id': process.env.clientIdSand,
  'client_secret': process.env.clientSecretSand
  
});

const app = express()

app.use(bodyParser.json())
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
)

app.set('view engine', 'ejs')

app.get('/', (req, res) => res.render('index'))

app.post('/pay', (req, res) => {

  /*
  const data  = req.body
  console.log(data)
  res.status(200).json({ error: false, msg: data })
  */
  
  var create_payment_json = {
    "intent": "sale",
    "payer": {
        "payment_method": "paypal"
    },
    "redirect_urls": {
        "return_url": "http://localhost:3000/success",
        "cancel_url": "http://localhost:3000/cancel"
    },
    "transactions": [{
        "item_list": {
            "items": [{
                "name": "Node Pay",
                "price": "100.00",
                "currency": "THB",
                "quantity": 1
            }]
        },
        "amount": {
            "currency": "THB",
            "total": "100.00"
        },
        "description": "Test."
    }]
  }

  paypal.payment.create(create_payment_json, function (error, payment) {
    if (error) {
        throw error;
    } else {
        console.log(payment);
        for(let i = 0; i < payment.links.length; i++) {
          if(payment.links[i].rel === 'approval_url') {
            res.redirect(payment.links[i].href)
          }
        }
    }
  })

})

app.get('/success', (req, res) => {
  const payerId = req.query.PayerID
  const paymentId = req.query.paymentId

  const execute_payment_json = {
    "payer_id": payerId,
    "transactions": [{
      "amount": {
        "currency": "THB",
        "total": "100.00"
      }
    }]
  }

  paypal.payment.execute(paymentId, execute_payment_json, function (error, payment) {
    try{
      if (error) {
        console.log(error.response)
        throw error
      } else {
        console.log("Get Payment Response")
        console.log(JSON.stringify(payment))
        res.send('Success')
      }
    }
    catch(error){
      res.send('Error')
    }
  })
})

app.get('/cancel', (req, res) => res.send('Cancelled'))

app.listen(3000, () => console.log('Server Started'))
